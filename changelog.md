# Daily Digest Widget
## 1.1.2
- Conditional widget title loading
## 1.1.1
- Update to widget header markup
- Update to title item size
## 1.1.0
- Improved error handling
- Improved accessibility